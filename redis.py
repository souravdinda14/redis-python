import redis

# Connect to Elasticache Redis
r = redis.Redis(host='redis-demo.uhd1ph.ng.0001.aps1.cache.amazonaws.com', port=6379)

# Fetch Redis instance info
redis_info = r.info()

# Get the used memory size in bytes
used_memory = redis_info['used_memory']

# Convert bytes to megabytes
used_memory_mb = used_memory / 1024 / 1024

print(f"Elasticache Redis size: {used_memory_mb:.2f} MB")